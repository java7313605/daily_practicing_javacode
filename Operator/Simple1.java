package JAVA.daily_practicing_javacode.Operator;

public class Simple1 {
    public static void main(String[] args){

        // number incre Decre  shows

        int a = 4;
        //int b = 5;

        System.out.println(a++);
        System.out.println(a);
        System.out.println("---------");
        System.out.println(++a);
        System.out.println(a);
        System.out.println("---------");
        System.out.println(a--);
        System.out.println(a);
        System.out.println("---------");
        System.out.println(--a);
        System.out.println(a);        
    }
}
