
// Program to Demonstrate automatic type conversion (implicit) and type casting (explicit).

class Conversion{

	public static void main(String[] args){
	
		byte b;
		int i = 500;
		float f =123.942f;
		System.out.println("\nConversion of float to byte.");
		b = (byte)f;
		System.out.println("float was "+f+" and byte is "+b);
		System.out.println("\n Conversion of int to float");
		f = i;
		System.out.println("integer was "+i+ " and float is "+f);
	}
}
