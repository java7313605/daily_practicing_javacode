
// find the square number is simply multiplied by itself and the result is stored in the variable"res".

import java.io.*;
class Square2{

	public static void main(String[] args)throws IOException{
	
		int i,res;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str;
		System.out.println("\n Enter the Number:");
		str = br.readLine();
		i= Integer.parseInt(str);
		res = i*i;
		System.out.println("The Square ="+res);

	}
}
